/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QWidget *centralWidget;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label_2;
    QRadioButton *radioButton;
    QLabel *label;
    QRadioButton *radioButton_2;
    QLabel *label_4;
    QLabel *label_11;
    QLabel *label_3;
    QPushButton *pushButton;
    QLabel *label_8;
    QPushButton *pushButton_4;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_5;
    QSpinBox *spinBox;
    QLabel *label_7;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_6;
    QPushButton *pushButton_3;
    QLabel *label_14;
    QLabel *label_10;
    QLabel *label_9;
    QPushButton *pushButton_5;
    QLabel *label_16;
    QLabel *label_15;
    QLabel *label_17;
    QPushButton *pushButton_2;
    QLabel *label_18;
    QLabel *label_19;
    QDoubleSpinBox *doubleSpinBox_2;
    QLabel *label_20;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QPushButton *pushButton_6;
    QLabel *label_24;
    QDoubleSpinBox *doubleSpinBox_3;
    QLabel *label_25;
    QLabel *label_26;
    QRadioButton *radioButton_5;
    QRadioButton *radioButton_6;
    QLabel *label_27;
    QDoubleSpinBox *doubleSpinBox_4;
    QLabel *label_28;
    QPushButton *pushButton_7;
    QLabel *label_29;
    QLabel *label_30;
    QLabel *label_31;
    QLabel *label_32;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QLabel *label_33;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(1180, 806);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MyPainterClass->sizePolicy().hasHeightForWidth());
        MyPainterClass->setSizePolicy(sizePolicy);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Ignored, QSizePolicy::Ignored);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(9, 9, 394, 761));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(10, 10, 10, 10);
        label_2 = new QLabel(layoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label_2);

        radioButton = new QRadioButton(layoutWidget);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setChecked(true);

        formLayout->setWidget(0, QFormLayout::FieldRole, radioButton);

        label = new QLabel(layoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label);

        radioButton_2 = new QRadioButton(layoutWidget);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        formLayout->setWidget(1, QFormLayout::FieldRole, radioButton_2);

        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        formLayout->setWidget(2, QFormLayout::LabelRole, label_4);

        label_11 = new QLabel(layoutWidget);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout->setWidget(2, QFormLayout::FieldRole, label_11);

        label_3 = new QLabel(layoutWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        formLayout->setWidget(3, QFormLayout::LabelRole, label_3);

        pushButton = new QPushButton(layoutWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(3, QFormLayout::FieldRole, pushButton);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout->setWidget(4, QFormLayout::LabelRole, label_8);

        pushButton_4 = new QPushButton(layoutWidget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        formLayout->setWidget(4, QFormLayout::FieldRole, pushButton_4);

        label_12 = new QLabel(layoutWidget);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout->setWidget(5, QFormLayout::LabelRole, label_12);

        label_13 = new QLabel(layoutWidget);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout->setWidget(5, QFormLayout::FieldRole, label_13);

        label_5 = new QLabel(layoutWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        formLayout->setWidget(6, QFormLayout::LabelRole, label_5);

        spinBox = new QSpinBox(layoutWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMinimum(3);
        spinBox->setMaximum(10000);

        formLayout->setWidget(6, QFormLayout::FieldRole, spinBox);

        label_7 = new QLabel(layoutWidget);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout->setWidget(7, QFormLayout::LabelRole, label_7);

        doubleSpinBox = new QDoubleSpinBox(layoutWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMinimum(0.01);
        doubleSpinBox->setValue(30);

        formLayout->setWidget(7, QFormLayout::FieldRole, doubleSpinBox);

        label_6 = new QLabel(layoutWidget);
        label_6->setObjectName(QStringLiteral("label_6"));

        formLayout->setWidget(8, QFormLayout::LabelRole, label_6);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        formLayout->setWidget(8, QFormLayout::FieldRole, pushButton_3);

        label_14 = new QLabel(layoutWidget);
        label_14->setObjectName(QStringLiteral("label_14"));

        formLayout->setWidget(9, QFormLayout::LabelRole, label_14);

        label_10 = new QLabel(layoutWidget);
        label_10->setObjectName(QStringLiteral("label_10"));

        formLayout->setWidget(9, QFormLayout::FieldRole, label_10);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QStringLiteral("label_9"));

        formLayout->setWidget(10, QFormLayout::LabelRole, label_9);

        pushButton_5 = new QPushButton(layoutWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        formLayout->setWidget(10, QFormLayout::FieldRole, pushButton_5);

        label_16 = new QLabel(layoutWidget);
        label_16->setObjectName(QStringLiteral("label_16"));

        formLayout->setWidget(11, QFormLayout::LabelRole, label_16);

        label_15 = new QLabel(layoutWidget);
        label_15->setObjectName(QStringLiteral("label_15"));

        formLayout->setWidget(11, QFormLayout::FieldRole, label_15);

        label_17 = new QLabel(layoutWidget);
        label_17->setObjectName(QStringLiteral("label_17"));

        formLayout->setWidget(12, QFormLayout::LabelRole, label_17);

        pushButton_2 = new QPushButton(layoutWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setEnabled(true);

        formLayout->setWidget(12, QFormLayout::FieldRole, pushButton_2);

        label_18 = new QLabel(layoutWidget);
        label_18->setObjectName(QStringLiteral("label_18"));

        formLayout->setWidget(14, QFormLayout::LabelRole, label_18);

        label_19 = new QLabel(layoutWidget);
        label_19->setObjectName(QStringLiteral("label_19"));

        formLayout->setWidget(14, QFormLayout::FieldRole, label_19);

        doubleSpinBox_2 = new QDoubleSpinBox(layoutWidget);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setMinimum(0.1);
        doubleSpinBox_2->setSingleStep(0.1);
        doubleSpinBox_2->setValue(1);

        formLayout->setWidget(13, QFormLayout::FieldRole, doubleSpinBox_2);

        label_20 = new QLabel(layoutWidget);
        label_20->setObjectName(QStringLiteral("label_20"));

        formLayout->setWidget(13, QFormLayout::LabelRole, label_20);

        radioButton_3 = new QRadioButton(layoutWidget);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));
        radioButton_3->setChecked(false);
        radioButton_3->setAutoExclusive(false);

        formLayout->setWidget(15, QFormLayout::FieldRole, radioButton_3);

        radioButton_4 = new QRadioButton(layoutWidget);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setAutoExclusive(false);

        formLayout->setWidget(16, QFormLayout::FieldRole, radioButton_4);

        label_21 = new QLabel(layoutWidget);
        label_21->setObjectName(QStringLiteral("label_21"));

        formLayout->setWidget(15, QFormLayout::LabelRole, label_21);

        label_22 = new QLabel(layoutWidget);
        label_22->setObjectName(QStringLiteral("label_22"));

        formLayout->setWidget(16, QFormLayout::LabelRole, label_22);

        label_23 = new QLabel(layoutWidget);
        label_23->setObjectName(QStringLiteral("label_23"));

        formLayout->setWidget(18, QFormLayout::LabelRole, label_23);

        pushButton_6 = new QPushButton(layoutWidget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        formLayout->setWidget(18, QFormLayout::FieldRole, pushButton_6);

        label_24 = new QLabel(layoutWidget);
        label_24->setObjectName(QStringLiteral("label_24"));

        formLayout->setWidget(17, QFormLayout::LabelRole, label_24);

        doubleSpinBox_3 = new QDoubleSpinBox(layoutWidget);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setMinimum(-99.99);
        doubleSpinBox_3->setSingleStep(0.1);
        doubleSpinBox_3->setValue(1);

        formLayout->setWidget(17, QFormLayout::FieldRole, doubleSpinBox_3);

        label_25 = new QLabel(layoutWidget);
        label_25->setObjectName(QStringLiteral("label_25"));

        formLayout->setWidget(19, QFormLayout::LabelRole, label_25);

        label_26 = new QLabel(layoutWidget);
        label_26->setObjectName(QStringLiteral("label_26"));

        formLayout->setWidget(19, QFormLayout::FieldRole, label_26);

        radioButton_5 = new QRadioButton(layoutWidget);
        radioButton_5->setObjectName(QStringLiteral("radioButton_5"));
        radioButton_5->setAutoExclusive(false);

        formLayout->setWidget(21, QFormLayout::FieldRole, radioButton_5);

        radioButton_6 = new QRadioButton(layoutWidget);
        radioButton_6->setObjectName(QStringLiteral("radioButton_6"));
        radioButton_6->setAutoExclusive(false);

        formLayout->setWidget(20, QFormLayout::FieldRole, radioButton_6);

        label_27 = new QLabel(layoutWidget);
        label_27->setObjectName(QStringLiteral("label_27"));

        formLayout->setWidget(20, QFormLayout::LabelRole, label_27);

        doubleSpinBox_4 = new QDoubleSpinBox(layoutWidget);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setDecimals(5);
        doubleSpinBox_4->setValue(3.14159);

        formLayout->setWidget(22, QFormLayout::FieldRole, doubleSpinBox_4);

        label_28 = new QLabel(layoutWidget);
        label_28->setObjectName(QStringLiteral("label_28"));

        formLayout->setWidget(22, QFormLayout::LabelRole, label_28);

        pushButton_7 = new QPushButton(layoutWidget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        formLayout->setWidget(23, QFormLayout::FieldRole, pushButton_7);

        label_29 = new QLabel(layoutWidget);
        label_29->setObjectName(QStringLiteral("label_29"));

        formLayout->setWidget(23, QFormLayout::LabelRole, label_29);

        label_30 = new QLabel(layoutWidget);
        label_30->setObjectName(QStringLiteral("label_30"));

        formLayout->setWidget(24, QFormLayout::LabelRole, label_30);

        label_31 = new QLabel(layoutWidget);
        label_31->setObjectName(QStringLiteral("label_31"));

        formLayout->setWidget(24, QFormLayout::FieldRole, label_31);

        label_32 = new QLabel(layoutWidget);
        label_32->setObjectName(QStringLiteral("label_32"));

        formLayout->setWidget(25, QFormLayout::LabelRole, label_32);

        pushButton_8 = new QPushButton(layoutWidget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));

        formLayout->setWidget(25, QFormLayout::FieldRole, pushButton_8);

        pushButton_9 = new QPushButton(layoutWidget);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));

        formLayout->setWidget(26, QFormLayout::FieldRole, pushButton_9);

        label_33 = new QLabel(layoutWidget);
        label_33->setObjectName(QStringLiteral("label_33"));

        formLayout->setWidget(26, QFormLayout::LabelRole, label_33);

        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setGeometry(QRect(409, 9, 761, 761));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 759, 759));
        scrollArea->setWidget(scrollAreaWidgetContents_3);
        MyPainterClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);

        retranslateUi(MyPainterClass);
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(vyberFarby()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliKruh()));
        QObject::connect(pushButton_4, SIGNAL(clicked()), MyPainterClass, SLOT(vyberFarbyVyplne()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), MyPainterClass, SLOT(vykresliMnohouholnik()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(skaluj()));
        QObject::connect(radioButton_3, SIGNAL(clicked()), MyPainterClass, SLOT(skosenieSmerX()));
        QObject::connect(radioButton_4, SIGNAL(clicked()), MyPainterClass, SLOT(skosenieSmerY()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), MyPainterClass, SLOT(skos()));
        QObject::connect(pushButton_7, SIGNAL(clicked()), MyPainterClass, SLOT(otoc()));
        QObject::connect(pushButton_9, SIGNAL(clicked()), MyPainterClass, SLOT(preklop()));
        QObject::connect(pushButton_8, SIGNAL(clicked()), MyPainterClass, SLOT(kresliOs()));
        QObject::connect(radioButton_6, SIGNAL(clicked()), MyPainterClass, SLOT(otocenieSmerHodiniek()));
        QObject::connect(radioButton_5, SIGNAL(clicked()), MyPainterClass, SLOT(otocenieProtiSmerHodiniek()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", Q_NULLPTR));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", Q_NULLPTR));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", Q_NULLPTR));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", Q_NULLPTR));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", Q_NULLPTR));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", Q_NULLPTR));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", Q_NULLPTR));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", Q_NULLPTR));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", Q_NULLPTR));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", Q_NULLPTR));
        label_2->setText(QApplication::translate("MyPainterClass", "DDA", Q_NULLPTR));
        radioButton->setText(QApplication::translate("MyPainterClass", "RadioButton", Q_NULLPTR));
        label->setText(QApplication::translate("MyPainterClass", "Bresenham", Q_NULLPTR));
        radioButton_2->setText(QApplication::translate("MyPainterClass", "RadioButton", Q_NULLPTR));
        label_4->setText(QApplication::translate("MyPainterClass", "-------------------------", Q_NULLPTR));
        label_11->setText(QApplication::translate("MyPainterClass", "----------------------------------------------------------------", Q_NULLPTR));
        label_3->setText(QApplication::translate("MyPainterClass", "Farba hran", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MyPainterClass", "QColorDialog", Q_NULLPTR));
        label_8->setText(QApplication::translate("MyPainterClass", "Farba vyplne", Q_NULLPTR));
        pushButton_4->setText(QApplication::translate("MyPainterClass", "QColorDialog", Q_NULLPTR));
        label_12->setText(QApplication::translate("MyPainterClass", "-------------------------", Q_NULLPTR));
        label_13->setText(QApplication::translate("MyPainterClass", "---------------------------------------------------------------", Q_NULLPTR));
        label_5->setText(QApplication::translate("MyPainterClass", "Po\304\215et \303\272se\304\215iek", Q_NULLPTR));
        label_7->setText(QApplication::translate("MyPainterClass", "Polomer", Q_NULLPTR));
        label_6->setText(QApplication::translate("MyPainterClass", "Vykresli kruh", Q_NULLPTR));
        pushButton_3->setText(QApplication::translate("MyPainterClass", "Kresli kruh", Q_NULLPTR));
        label_14->setText(QApplication::translate("MyPainterClass", "--------------------------", Q_NULLPTR));
        label_10->setText(QApplication::translate("MyPainterClass", "----------------------------------------------------------------", Q_NULLPTR));
        label_9->setText(QApplication::translate("MyPainterClass", "Vykresli mnohouholn\303\255k", Q_NULLPTR));
        pushButton_5->setText(QApplication::translate("MyPainterClass", "Kresli mnohouholn\303\255k", Q_NULLPTR));
        label_16->setText(QApplication::translate("MyPainterClass", "--------------------------", Q_NULLPTR));
        label_15->setText(QApplication::translate("MyPainterClass", "-----------------------------------------------------------------", Q_NULLPTR));
        label_17->setText(QApplication::translate("MyPainterClass", "\305\240k\303\241luj objekt", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "\305\240k\303\241luj", Q_NULLPTR));
        label_18->setText(QApplication::translate("MyPainterClass", "--------------------------", Q_NULLPTR));
        label_19->setText(QApplication::translate("MyPainterClass", "----------------------------------------------------------------", Q_NULLPTR));
        label_20->setText(QApplication::translate("MyPainterClass", "Ve\304\276kos\305\245 \305\241kaly", Q_NULLPTR));
        radioButton_3->setText(QString());
        radioButton_4->setText(QString());
        label_21->setText(QApplication::translate("MyPainterClass", "Smere x", Q_NULLPTR));
        label_22->setText(QApplication::translate("MyPainterClass", "Smere y", Q_NULLPTR));
        label_23->setText(QApplication::translate("MyPainterClass", "Skosenie", Q_NULLPTR));
        pushButton_6->setText(QApplication::translate("MyPainterClass", "Skos", Q_NULLPTR));
        label_24->setText(QApplication::translate("MyPainterClass", "Hodnota", Q_NULLPTR));
        label_25->setText(QApplication::translate("MyPainterClass", "--------------------------", Q_NULLPTR));
        label_26->setText(QApplication::translate("MyPainterClass", "----------------------------------------------------------------", Q_NULLPTR));
        radioButton_5->setText(QApplication::translate("MyPainterClass", "Proti smeru hodinov\303\275ch ru\304\215i\304\215iek", Q_NULLPTR));
        radioButton_6->setText(QApplication::translate("MyPainterClass", "V smere hodinov\303\275ch ru\304\215i\304\215iek", Q_NULLPTR));
        label_27->setText(QString());
        label_28->setText(QApplication::translate("MyPainterClass", "Uhol v radi\303\241noch", Q_NULLPTR));
        pushButton_7->setText(QApplication::translate("MyPainterClass", "Oto\304\215 okolo bodu", Q_NULLPTR));
        label_29->setText(QApplication::translate("MyPainterClass", "Oto\304\215enie", Q_NULLPTR));
        label_30->setText(QApplication::translate("MyPainterClass", "---------------------------", Q_NULLPTR));
        label_31->setText(QApplication::translate("MyPainterClass", "----------------------------------------------------------------", Q_NULLPTR));
        label_32->setText(QApplication::translate("MyPainterClass", "Nakresli os", Q_NULLPTR));
        pushButton_8->setText(QApplication::translate("MyPainterClass", "Kreslenie osi preklopenia", Q_NULLPTR));
        pushButton_9->setText(QApplication::translate("MyPainterClass", "Preklop", Q_NULLPTR));
        label_33->setText(QApplication::translate("MyPainterClass", "Preklopenie", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
