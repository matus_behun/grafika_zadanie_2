#include "paintwidget.h"

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
}

void PaintWidget::vykresliOznacenie(grafickyUtvar *u, QColor c) 
{
	vykresliVertikalnuCiaru(u->dajLaveDolneOhranicenie().y(), u->dajPraveHorneOhranicenie().y(), u->dajLaveDolneOhranicenie().x(), c);
	vykresliVertikalnuCiaru(u->dajLaveDolneOhranicenie().y(), u->dajPraveHorneOhranicenie().y(), u->dajPraveHorneOhranicenie().x(), c);
	vykresliVodorovnuCiaru(u->dajLaveDolneOhranicenie().x(), u->dajPraveHorneOhranicenie().x(), u->dajLaveDolneOhranicenie().y(), c);
	vykresliVodorovnuCiaru(u->dajLaveDolneOhranicenie().x(), u->dajPraveHorneOhranicenie().x(), u->dajPraveHorneOhranicenie().y(), c);
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		int i;
		for (i = 0; i < vykresleneUtvary.size(); i++) {
				/* Klikol som vnutry nejakeho utvaru */
			if (vykresleneUtvary[i]->jeVnutriOhranicenia(event->pos())) {
					/* Klikol som na iny objekt ako ozaceny */
				if (oznaceny) {
					vykresliOznacenie(oznaceny, Qt::white);
				}
				oznaceny = vykresleneUtvary.at(i);
				vykresliOznacenie(oznaceny, farbaOhranicenia);
				vykresleneUtvary.swap(0, i);
				break;
			}
		}

			/* Klikol som mimo oznaceneho utvaru a nejaky mam oznaceny */
		if (oznaceny) {
			if (i == vykresleneUtvary.size()) {
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny = NULL;
			}
		}
		if (maOs) {
			zmazOs();
		}

		clicks.append(event->pos());
		lastMovedMousePosition = event->pos();

	}
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());

	update();

	return true;
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::setColor(QColor c)
{
	color = c;
}

void PaintWidget::setFillingColor(QColor c)
{
	fillingColor = c;
}

 void PaintWidget::kresliCiaru(char algoritmus)
{
	if (clicks.size() > 1) {
		
	}
	if (algoritmus = 'd') {
		dda(clicks.last(), clicks.at(clicks.size() - 2), color);
	} else if(algoritmus = 'b') {
		bresenham(clicks.last(), clicks.at(clicks.size() - 2), color);
	}

	clicks.clear();
}

void PaintWidget::dda(QPoint a, QPoint b, QColor c)
{
	QPoint m, n, kurzor;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	}
	else if(a.x() > b.x()){
		m = b;
		n = a;
	} else {
		vykresliVertikalnuCiaru(a.y(), b.y(), a.x(), c);
		return;
	}

	QPainter painter(&image);
	painter.setPen(QPen(c));

	double delta_x, delta_y, dlzka_x, dlzka_y, dkurzor_x, dkurzor_y;

	kurzor = m;
	dkurzor_x = (double)m.x();
	dkurzor_y = (double)m.y();

	dlzka_x = fabs(m.x() - n.x());
	dlzka_y = fabs(m.y() - n.y());

	if (dlzka_y <= dlzka_x) {
		delta_y = dlzka_y / dlzka_x;
		delta_x = 1;
	}
	else {
		delta_y = 1;
		delta_x = dlzka_x / dlzka_y;
	}

	if (m.y() > n.y()) {
		delta_y *= -1;
	}

	do {
		painter.drawPoint(kurzor.x(), kurzor.y());
		dkurzor_x += delta_x;
		dkurzor_y += delta_y;
		kurzor.setX((int) round(dkurzor_x));
		kurzor.setY((int) round(dkurzor_y));
	} while (kurzor.x() != n.x() || kurzor.y() != n.y());
	painter.drawPoint(kurzor.x(), kurzor.y());

	update();
}

void PaintWidget::bresenham(QPoint a, QPoint b, QColor c)
{
	QPainter painter(&image);
	painter.setPen(QPen(c));

	QPoint m, n, kurzor;
	int delta_x, delta_y, aktualny, nasledujuci, smer_y, matrix_size;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	} else if(a.x() > b.x()) {
		m = b;
		n = a;
	} else {
		vykresliVertikalnuCiaru(a.y(), b.y(), a.x(), c);
		return;
	}

	kurzor = m;

	delta_x = abs(m.x() - n.x());
	delta_y = abs(m.y() - n.y());

	if (m.y() > n.y()) {
		smer_y = -1;
		matrix_size = 0; // pridat velkost Y obrazka;
	}
	else {
		smer_y = 1;
		matrix_size = 2 * kurzor.y();
	}

	if (delta_y < delta_x) {
		aktualny = 2 * delta_y * (kurzor.x() + 1) - 2 * delta_x * kurzor.y() + delta_x * (2 * m.y() - 1);
	}
	else {
		aktualny = 2 * delta_x * ((matrix_size - kurzor.y()) + 1) - 2 * delta_y * kurzor.x() + delta_y * (2 * m.x() - 1);
	}

	while (kurzor.x() != n.x() || kurzor.y() != n.y()) {
		painter.drawPoint(kurzor.x(), kurzor.y());
		if (delta_y < delta_x) {
			if (aktualny <= 0) {
				nasledujuci = aktualny + 2 * delta_y;
			}
			else {
				nasledujuci = aktualny + 2 * delta_y - 2 * delta_x;
				kurzor.setY(kurzor.y() + smer_y);
			}
			kurzor.setX(kurzor.x() + 1);
		}
		else {
			if (aktualny < 0) {
				nasledujuci = aktualny + 2 * delta_x;
			}
			else {
				nasledujuci = aktualny + 2 * delta_x - 2 * delta_y;
				kurzor.setX(kurzor.x() + 1);
			}
			kurzor.setY(kurzor.y() + smer_y);
		}
		aktualny = nasledujuci;
	}
	painter.drawPoint(kurzor.x(), kurzor.y());

	update();
}

void PaintWidget::vykresliVodorovnuCiaru(int x, int y, int uroven, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(farba));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else {
		koniec = x;
		zaciatok = y;
	}
	
	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(i, uroven);
	}

	update();
}

void PaintWidget::vykresliVertikalnuCiaru(int x, int y, int uroven, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(farba));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else if(x > y) {
		koniec = x;
		zaciatok = y;
	} else {
		return;
	}

	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(uroven, i);
	}

	update();
}

void PaintWidget::kresliKruh(int pocetUseciek, char algoritmus, double polomer) 
{
	QList<hrana> hranyVypln;

	if (clicks.empty()) {
		return;
	}

	kruh k(clicks.last(), color, fillingColor, polomer, pocetUseciek, algoritmus);

		/* Vyplnanie polygonu */
	for (int i = 0; i < k.hrany.size(); i++) {
		if (!hrana::jeVodorovna(k.hrany[i].dajA(), k.hrany[i].dajB())) {
			hranyVypln.append(k.hrany.at(i));
		}
	}
	vyplnUzavretuOblast(hranyVypln, fillingColor);

	for (int i = 0; i < k.hrany.size(); i++)
	{
		if (k.dajAlgoritmus() == 'd') {
			dda(k.hrany[i].dajA(), k.hrany[i].dajB(), color);
		} else if(algoritmus == 'b') {
			bresenham(k.hrany[i].dajA(), k.hrany[i].dajB(), color);
		}
	}

	vykresleneKruhy.append(k);
	grafickyUtvar *g = (grafickyUtvar *)&(vykresleneKruhy.last());
	vykresleneUtvary.append(g);

	clicks.clear();
}

void PaintWidget::kresliKruh(kruh k, QColor farbaHran, QColor farbaVyplne) 
{
	QList<hrana> hranyVypln;

		/* Vyplnanie polygonu */
	for (int i = 0; i < k.hrany.size(); i++) {
		if (!hrana::jeVodorovna(k.hrany[i].dajA(), k.hrany[i].dajB())) {
			hranyVypln.append(k.hrany.at(i));
		}
	}
	vyplnUzavretuOblast(hranyVypln, farbaVyplne);

	for (int i = 0; i < k.hrany.size(); i++)
	{
		if (k.dajAlgoritmus() == 'd') {
			dda(k.hrany[i].dajA(), k.hrany[i].dajB(), farbaHran);
		} else if(k.dajAlgoritmus() == 'b') {
			bresenham(k.hrany[i].dajA(), k.hrany[i].dajB(), farbaHran);
		}
	}
}

bool cmp_dolneY_dolneX_smernica(const hrana &a, const hrana &b)
{
	return (a.dajDolneY() > b.dajDolneY()) ||
		   ((a.dajDolneY() == b.dajDolneY()) && (a.dajDolneX() < b.dajDolneX())) ||
		   ((a.dajDolneY() == b.dajDolneY()) && (a.dajDolneX() == b.dajDolneX()) && (a.dajSmernicu() < b.dajSmernicu()));
}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
	if ((event->buttons() & Qt::LeftButton)) {
		if (oznaceny && oznaceny->jeVnutriOhranicenia(lastMovedMousePosition)) {
				/* Presuvanie objektu */
			if (QString("polygon") == oznaceny->vypisDruh()) {
				/* Vybielenie originalnej pozicie*/
				for (int i = 0; i < vykreslenePolygony.size(); i++) {
					if (oznaceny == &(vykreslenePolygony.at(i))) {
						kresliMnohouholnik(vykreslenePolygony.at(i), Qt::white, Qt::white);
						vykresliOznacenie(oznaceny, Qt::white);
						oznaceny->presunutie(lastMovedMousePosition, event->pos());
						kresliMnohouholnik(vykreslenePolygony.at(i), vykreslenePolygony[i].dajFarbaHran(), vykreslenePolygony[i].dajFarbaVyplne());
						vykresliOznacenie(oznaceny, farbaOhranicenia);
						break;
					}
				}
				/* Prekreslenie novej pozicie */
			} else if (QString("kruh") == oznaceny->vypisDruh()) {
				for (int i = 0; i < vykresleneKruhy.size(); i++) {
					if (oznaceny == &(vykresleneKruhy.at(i))) {
						kresliKruh(vykresleneKruhy.at(i), Qt::white, Qt::white);
						vykresliOznacenie(oznaceny, Qt::white);
						oznaceny->presunutie(lastMovedMousePosition, event->pos());
						kresliKruh(vykresleneKruhy.at(i), vykresleneKruhy[i].dajFarbuHran(), vykresleneKruhy[i].dajFarbuVyplne());
						vykresliOznacenie(oznaceny, farbaOhranicenia);
						break;
					}
				}
			}
		}
	}
	lastMovedMousePosition = event->pos();
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
	}
}

void PaintWidget::vyplnUzavretuOblast(QList<hrana> hrany, QColor c)
{
	QList<hrana> aktivneHrany;

	qSort(hrany.begin(), hrany.end(), cmp_dolneY_dolneX_smernica);

		/* Nastav na hranu s najvacsim Y */
	int urovenY = hrany.first().dajDolneY() - 1;
	QMutableListIterator<hrana> aktivneHranyIter(aktivneHrany);
	QVector<int> priesecniky;
	int hranyIter = 0;
	do {
				/* Pridavanie hran k aktivnym */
			for (; hranyIter < hrany.size() && (hrany[hranyIter].dajDolneY() -1) == urovenY; hranyIter++) {
					/* Pridavam prvu hranu do zoznamu aktivnych hran */
				if (aktivneHrany.empty()) {
					aktivneHrany << hrany[hranyIter];
					/* Pridavam do neprazdneho zoznamu */
				} else {
					int miestoVlozenia;
						/* Najdi miesto vlozenia a vloz */
					for (miestoVlozenia = 0; miestoVlozenia < aktivneHrany.size() && hrany[hranyIter].dajPriesecnik() > round(aktivneHrany[miestoVlozenia].dajPriesecnik()); miestoVlozenia++) 
						;
					aktivneHrany.insert(miestoVlozenia, hrany.at(hranyIter));
				}
			}

				/* Vymazavanie z aktivnych hran */
			aktivneHranyIter.toFront();
			while (aktivneHranyIter.hasNext()) {
				if (aktivneHranyIter.next().dajHorneY() > urovenY) {
					aktivneHranyIter.remove();
				}
			}


				/* Vytvaranie a aktualizacia priesecnikov */
			aktivneHranyIter.toFront();
			while(aktivneHranyIter.hasNext()) {
				priesecniky << int(aktivneHranyIter.next().aktualizujPriesecnik());
			}

			qSort(priesecniky);

				/* Vykreslovanie horizontalnych ciar */
			for (int i = 0; i < priesecniky.size() - 1; i = i + 2) {
				vykresliVodorovnuCiaru(priesecniky[i], priesecniky[i + 1], urovenY, c);
			}
			priesecniky.clear();

		urovenY--;
	} while(!aktivneHrany.empty());
}

void PaintWidget::kresliMnohouholnik(char algoritmus)
{
	if (clicks.size() < 3) {
		return;
	}

	QList<hrana> hranyVypln;
	polygon p(clicks, color, fillingColor, algoritmus);

	vykreslenePolygony.append(p);
	grafickyUtvar *g = (grafickyUtvar *)(&(vykreslenePolygony.last()));
	vykresleneUtvary.append(g);

		/* Vyplnanie polygonu */
	for (int i = 0; i < p.hrany.size(); i++) {
		if (!hrana::jeVodorovna(p.hrany[i].dajA(), p.hrany[i].dajB())) {
			hranyVypln.append(p.hrany.at(i));
		}
	}
	vyplnUzavretuOblast(hranyVypln, fillingColor);

		/* Vykreslovanie hran polygonu */
	for (int i = 0; i < p.hrany.size(); i++) {
		if (algoritmus == 'd') {
			dda(p.hrany[i].dajA(), p.hrany[i].dajB(), color);
		} else if(algoritmus = 'b') {
			bresenham(p.hrany[i].dajA(), p.hrany[i].dajB(), color);
		}
	}

	clicks.clear();
}

void PaintWidget::skaluj(double velkost)
{
	if (!oznaceny) {
		return;
	}

	if (clicks.size() < 2) {
		return;
	}

	QPoint bodSkalovania = clicks.at(clicks.size() - 2);

	if (QString("polygon") == oznaceny->vypisDruh()) {
		/* Vybielenie originalnej pozicie*/
		for (int i = 0; i < vykreslenePolygony.size(); i++) {
			if (oznaceny == &(vykreslenePolygony.at(i))) {
				kresliMnohouholnik(vykreslenePolygony.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->skalovanie(bodSkalovania, velkost);
				kresliMnohouholnik(vykreslenePolygony.at(i), vykreslenePolygony[i].dajFarbaHran(), vykreslenePolygony[i].dajFarbaVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
			}
		}
		/* Prekreslenie novej pozicie */
	} else if (QString("kruh") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykresleneKruhy.size(); i++) {
			if (oznaceny == &(vykresleneKruhy.at(i))) {
				kresliKruh(vykresleneKruhy.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->skalovanie(bodSkalovania, velkost);
				kresliKruh(vykresleneKruhy.at(i), vykresleneKruhy[i].dajFarbuHran(), vykresleneKruhy[i].dajFarbuVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
			}
		}
	}

	clicks.clear();
}

void PaintWidget::skos(char os, double velkost)
{
	if (!oznaceny) {
		return;
	}

	if (clicks.size() < 2) {
		return;
	}

	QPoint osSkalovania = clicks.at(clicks.size() - 2);

	if (QString("polygon") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykreslenePolygony.size(); i++) {
			if (oznaceny == &(vykreslenePolygony.at(i))) {
				kresliMnohouholnik(vykreslenePolygony.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->skosenie(osSkalovania, os, velkost);
				kresliMnohouholnik(vykreslenePolygony.at(i), vykreslenePolygony[i].dajFarbaHran(), vykreslenePolygony[i].dajFarbaVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
			}
		}
	} else if (QString("kruh") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykresleneKruhy.size(); i++) {
			if (oznaceny == &(vykresleneKruhy.at(i))) {
				kresliKruh(vykresleneKruhy.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->skosenie(osSkalovania, os, velkost);
				kresliKruh(vykresleneKruhy.at(i), vykresleneKruhy[i].dajFarbuHran(), vykresleneKruhy[i].dajFarbuVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);

			}
		}
	}
	clicks.clear();
}

void PaintWidget::otoc(char smerOtocenia, double velkostOtocenia)
{
	if (!oznaceny) {
		return;
	}

	if (clicks.size() < 2) {
		return;
	}

	QPoint osRotacie = clicks.at(clicks.size() - 2);

	if (QString("polygon") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykreslenePolygony.size(); i++) {
			if (oznaceny == &(vykreslenePolygony.at(i))) {
				kresliMnohouholnik(vykreslenePolygony.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->otocenie(osRotacie, smerOtocenia, velkostOtocenia);
				kresliMnohouholnik(vykreslenePolygony.at(i), vykreslenePolygony[i].dajFarbaHran(), vykreslenePolygony[i].dajFarbaVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
				break;
			}
		}
	} else if (QString("kruh") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykresleneKruhy.size(); i++) {
			if (oznaceny == &(vykresleneKruhy.at(i))) {
				kresliKruh(vykresleneKruhy.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->otocenie(osRotacie, smerOtocenia, velkostOtocenia);
				kresliKruh(vykresleneKruhy.at(i), vykresleneKruhy[i].dajFarbuHran(), vykresleneKruhy[i].dajFarbuVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
				break;
			}
		}
	}
}

void PaintWidget::kresliOs(char algoritmus)
{
	if (clicks.size() < 2) {
		return;
	}

	if (maOs) {
		if (algoritmus == 'd') {
			zmazOs('d');
		} else if (algoritmus == 'b') {
			zmazOs('b');
		}
	}

	os.zmenA(clicks.last());
	os.zmenB(clicks.at(clicks.size() - 2));
	kresliCiaru(algoritmus);
	printf("clicks.last(): [%d %d]\n", clicks.last().x(), clicks.last().y());
	printf("clicks.at(clicks.size() - 2): [%d %d]\n", clicks.at(clicks.size() - 2).x(), clicks.at(clicks.size() - 2).y());

	printf("kreslim os: [%d %d] [%d %d]\n", os.dajA().x(), os.dajA().y(), os.dajB().x(), os.dajB().y());
	maOs = true;
}

void PaintWidget::zmazOs(char algoritmus)
{
	printf("mazem os: [%d %d] [%d %d]\n", os.dajA().x(), os.dajA().y(), os.dajB().x(), os.dajB().y());
	if (algoritmus == 'd') {
		dda(os.dajA(), os.dajB(), Qt::white);
	}
	else if (algoritmus == 'b') {
		bresenham(os.dajA(), os.dajB(), Qt::white);
	}
	maOs = false;
}

void PaintWidget::preklop(void)
{
	if (!oznaceny || !maOs) {
		return;
	}

	if (QString("polygon") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykreslenePolygony.size(); i++) {
			if (oznaceny == &(vykreslenePolygony.at(i))) {
				kresliMnohouholnik(vykreslenePolygony.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->preklopenie(os);
				kresliMnohouholnik(vykreslenePolygony.at(i), vykreslenePolygony[i].dajFarbaHran(), vykreslenePolygony[i].dajFarbaVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
				break;
			}
		}
	} else if (QString("kruh") == oznaceny->vypisDruh()) {
		for (int i = 0; i < vykresleneKruhy.size(); i++) {
			if (oznaceny == &(vykresleneKruhy.at(i))) {
				kresliKruh(vykresleneKruhy.at(i), Qt::white, Qt::white);
				vykresliOznacenie(oznaceny, Qt::white);
				oznaceny->preklopenie(os);
				kresliKruh(vykresleneKruhy.at(i), vykresleneKruhy[i].dajFarbuHran(), vykresleneKruhy[i].dajFarbuVyplne());
				vykresliOznacenie(oznaceny, farbaOhranicenia);
				break;
			}
		}
	}
}

void PaintWidget::kresliMnohouholnik(polygon p, QColor farbaHrany, QColor farbaVypln)
{
	QList<hrana> hranyVypln;

		/* Vyplnanie polygonu */
	for (int i = 0; i < p.hrany.size(); i++) {
		if (!hrana::jeVodorovna(p.hrany[i].dajA(), p.hrany[i].dajB())) {
			hranyVypln.append(p.hrany.at(i));
		}
	}
	vyplnUzavretuOblast(hranyVypln, farbaVypln);

		/* Vykreslovanie hran polygonu */
	for (int i = 0; i < p.hrany.size(); i++) {
		if (p.dajPouzityAlgoritmus() == 'd') {
			dda(p.hrany[i].dajA(), p.hrany[i].dajB(), farbaHrany);
		} else if(p.dajPouzityAlgoritmus() == 'b') {
			bresenham(p.hrany[i].dajA(), p.hrany[i].dajB(), farbaHrany);
		}
	}
}
