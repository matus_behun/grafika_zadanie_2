#pragma once
#include "grafickyUtvar.h"
#include <QList>
#include <QPoint>
#include <QColor>

class polygon :
	public grafickyUtvar
{
	public:
		polygon(QVector<QPoint>, QColor, QColor, char);
		~polygon();
		char dajPouzityAlgoritmus(void);
		QPoint dajLaveDolneOhranicenie(void);
		QPoint dajPraveHorneOhranicenie(void);
		QColor dajFarbaHran(void);
		QColor dajFarbaVyplne(void);
		void presunutie(QPoint, QPoint);
		QString vypisDruh(void);
	private: 
		char pouzityAlgoritmus;
		QColor farbaHran;
		QColor farbaVyplne;
};

