#include "polygon.h"

polygon::polygon(QVector<QPoint> hrany, QColor farbaHran, QColor farbaVyplne, char algoritmus)
{
	this->farbaHran = farbaHran;
	this->farbaVyplne = farbaVyplne;
	pouzityAlgoritmus = algoritmus;
	for (int i = 0; i < hrany.size() - 1; i++) {
		hrana h(hrany.at(i), hrany.at(i + 1));
		this->hrany.append(h);
	}

	hrana h(hrany.first(), hrany.last());
	this->hrany.append(h);
	vypocitajOhranicenia();
}

char polygon::dajPouzityAlgoritmus(void)
{
	return pouzityAlgoritmus;
}

QPoint polygon::dajLaveDolneOhranicenie(void)
{
	return laveDolneOhranicenie;
}

QPoint polygon::dajPraveHorneOhranicenie(void)
{
	return praveHorneOhranicenie;
}

QColor polygon::dajFarbaHran(void)
{
	return farbaHran;
}

QColor polygon::dajFarbaVyplne(void)
{
	return farbaVyplne;
}

void polygon::presunutie(QPoint originalnaPozicia, QPoint novaPozicia)
{
	int deltaX = novaPozicia.x() - originalnaPozicia.x();
	int deltaY = novaPozicia.y() - originalnaPozicia.y();

	for (int i = 0; i < hrany.size(); i++) {
		hrany[i].presunX(deltaX);
		hrany[i].presunY(deltaY);
	}

	vypocitajOhranicenia();
}

QString polygon::vypisDruh(void)
{
	return QString("polygon");
}

polygon::~polygon()
{
}