#pragma once
#include "grafickyUtvar.h"
#include "hrana.h"
#include <QPoint>
#include <QColor>
#include <math.h>

#ifndef M_PI
	#define M_PI (3.14159265358979323846)
#endif

class kruh : public grafickyUtvar
{
	public:
		kruh(QPoint, QColor, QColor, double, int, char);
		char dajAlgoritmus(void);
		QPoint dajLaveDolneOhranicenie(void);
		QPoint dajPraveHorneOhranicenie(void);
		QColor dajFarbuHran(void);
		QColor dajFarbuVyplne(void);
		void presunutie(QPoint, QPoint);
		QString vypisDruh(void);
		~kruh();
	private: 
		QPoint stred;
		QColor farbaHran;
		QColor farbaVyplne;
		double polomer;
		int pocetUseciek;
		char pouzityAlgoritmus;
};

