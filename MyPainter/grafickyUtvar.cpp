#include "grafickyUtvar.h"

grafickyUtvar::grafickyUtvar()
{
}

grafickyUtvar::~grafickyUtvar()
{
}

bool grafickyUtvar::jeVnutriOhranicenia(QPoint a)
{
	if (a.x() > laveDolneOhranicenie.x() && a.x() < praveHorneOhranicenie.x() && a.y() < laveDolneOhranicenie.y() && a.y() > praveHorneOhranicenie.y()) {
		return true;
	} else {
		return false;
	}
}

void grafickyUtvar::skalovanie(QPoint a, double velkost)
{
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m = hrany[i].dajA();
		QPoint n = hrany[i].dajB();

		QPoint relativnyVektor = m - a;
		relativnyVektor *= velkost;
		m = a + relativnyVektor;

		relativnyVektor = n - a;
		relativnyVektor *= velkost;
		n= a + relativnyVektor;

		hrany[i].nastavHranu(m, n);
	}
	vypocitajOhranicenia();
}

void grafickyUtvar::skosenie(QPoint a, char os, double velkost)
{
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m = hrany[i].dajA();
		QPoint n = hrany[i].dajB();
		if (os == 'x') {
			m.setX(m.x() + velkost * (a.y() - m.y()));
			n.setX(n.x() + velkost * (a.y() - n.y()));
		} else if (os == 'y') {
			m.setY(m.y() + velkost * (m.x() - a.x()));
			n.setY(n.y() + velkost * (n.x() - a.x()));
		}
		hrany[i].nastavHranu(m, n);
	}
	vypocitajOhranicenia();
}

void grafickyUtvar::otocenie(QPoint os, char smer, double velkost)
{
	printf("%lf", velkost);
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m, n;
		double deltaXA = double(double(hrany[i].dajA().x()) - double(os.x()));
		double deltaYA = double(double(hrany[i].dajA().y()) - double(os.y()));
		double deltaXB = double(double(hrany[i].dajB().x()) - double(os.x()));
		double deltaYB = double(double(hrany[i].dajB().y()) - double(os.y()));

		if (smer == 'p') {
			m.setX(round(deltaXA * cos(velkost) - deltaYA * sin(velkost) + os.x()));
			m.setY(round(deltaXA * sin(velkost) + deltaYA * cos(velkost) + os.y()));

			n.setX(round(deltaXB * cos(velkost) - deltaYB * sin(velkost) + os.x()));
			n.setY(round(deltaXB * sin(velkost) + deltaYB * cos(velkost) + os.y()));
		} else if(smer == 'v') {
			m.setX(round(deltaXA * cos(velkost) + deltaYA * sin(velkost) + os.x()));
			m.setY(round(-deltaXA * sin(velkost) + deltaYA * cos(velkost) + os.y()));

			n.setX(round(deltaXB * cos(velkost) + deltaYB * sin(velkost) + os.x()));
			n.setY(round(-deltaXB * sin(velkost) + deltaYB * cos(velkost) + os.y()));
		}

		hrany[i].nastavHranu(m, n);
	}
	vypocitajOhranicenia();
}

void grafickyUtvar::preklopenie(hrana os)
{
	for (int i = 0; i < hrany.size(); i++) {
		QPoint m, n;
		int a, b, c;


		hrany[i].nastavHranu(m, n);
	}
	vypocitajOhranicenia();
}

void grafickyUtvar::vypocitajOhranicenia(void)
{
	int minX = INT_MAX, minY = INT_MAX, maxX = 0, maxY = 0;

	for (int i = 0; i < hrany.size(); i++) {
		if (hrany[i].dajHorneY() < minY) {
			minY = hrany[i].dajHorneY();
		}
		if (hrany[i].dajDolneY() > maxY) {
			maxY = hrany[i].dajDolneY();
		}
		if (hrany[i].dajA().x() < hrany[i].dajB().x()) {
			if (hrany[i].dajA().x() < minX) {
				minX = hrany[i].dajA().x();
			}
			if (hrany[i].dajB().x() > maxX) {
				maxX = hrany[i].dajB().x();
			}
		}
		else {
			if (hrany[i].dajB().x() < minX) {
				minX = hrany[i].dajB().x();
			}
			if (hrany[i].dajA().x() > maxX) {
				maxX = hrany[i].dajA().x();
			}
		}
	}

	laveDolneOhranicenie.setX(minX - 3);
	laveDolneOhranicenie.setY(maxY + 3);
	praveHorneOhranicenie.setX(maxX + 3);
	praveHorneOhranicenie.setY(minY - 3);
}
