#pragma once

#include <QObject>
#include <QPoint>
#include <QVector>
#include <math.h>
#include "hrana.h"

#ifndef M_PI
	#define M_PI (3.14159265358979323846)
#endif


class grafickyUtvar
{
	public:
		grafickyUtvar();
		~grafickyUtvar();
		QList<hrana> hrany;
		bool jeVnutriOhranicenia(QPoint);
		virtual QPoint dajLaveDolneOhranicenie(void) = 0;
		virtual QPoint dajPraveHorneOhranicenie(void) = 0;
		virtual void presunutie(QPoint, QPoint) = 0;
		virtual QString vypisDruh(void) = 0;
		void skalovanie(QPoint, double);
		void skosenie(QPoint, char, double);
		void otocenie(QPoint, char, double);
		void preklopenie(hrana);
	protected:
		void vypocitajOhranicenia(void);
		QPoint laveDolneOhranicenie;
		QPoint praveHorneOhranicenie;
	private: 
		
};
