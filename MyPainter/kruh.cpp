#include "kruh.h"

kruh::kruh(QPoint stred, QColor farbaHran, QColor farbaVyplne, double polomer, int pocetUseciek, char algoritmus)
{
	this->stred = stred;
	this->farbaHran = farbaHran;
	this->farbaVyplne = farbaVyplne;
	this->polomer = polomer;
	this->pocetUseciek = pocetUseciek;
	this->pouzityAlgoritmus = algoritmus;

	for (int i = 0; i < pocetUseciek; i++)
	{
		QPoint a, b;
		a.setX(round(stred.x() + polomer * cos((i * 2 * M_PI) / pocetUseciek)));
		a.setY(round(stred.y() + polomer * sin((i * 2 * M_PI) / pocetUseciek)));
		b.setX(round(stred.x() + polomer * cos(((i + 1) * 2 * M_PI) / pocetUseciek)));
		b.setY(round(stred.y() + polomer * sin(((i + 1) * 2 * M_PI) / pocetUseciek)));
		
		hrana h(a, b);
		hrany.append(h);
	}
	vypocitajOhranicenia();
}

char kruh::dajAlgoritmus(void)
{
	return pouzityAlgoritmus;
}

QPoint kruh::dajLaveDolneOhranicenie(void)
{
	return laveDolneOhranicenie;
}

QPoint kruh::dajPraveHorneOhranicenie(void)
{
	return praveHorneOhranicenie;
}

QColor kruh::dajFarbuHran(void)
{
	return farbaHran;
}

QColor kruh::dajFarbuVyplne(void)
{
	return farbaVyplne;
}

void kruh::presunutie(QPoint originalnaPozicia, QPoint novaPozicia)
{
	int deltaY = novaPozicia.y() - originalnaPozicia.y();
	int deltaX = novaPozicia.x() - originalnaPozicia.x();

	for (int i = 0; i < hrany.size(); i++) {
		hrany[i].presunX(deltaX);
		hrany[i].presunY(deltaY);
	}

	stred.setY(stred.y() + deltaY);
	stred.setX(stred.x() + deltaX);

	vypocitajOhranicenia();
}

QString kruh::vypisDruh(void)
{
	return QString("kruh");
}

kruh::~kruh()
{
}
