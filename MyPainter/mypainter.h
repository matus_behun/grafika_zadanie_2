#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include "ui_mypainter.h"
#include "paintwidget.h"
#include <QColorDialog>
#include "hrana.h"

class MyPainter : public QMainWindow
{
	Q_OBJECT

public:
	MyPainter(QWidget *parent = 0);
	~MyPainter();

public slots:
	void vyberFarby();
	void vyberFarbyVyplne();
	void vykresliCiaru();
	void vykresliKruh();
	void vykresliMnohouholnik(); 
	void skaluj();
	void skosenieSmerX();
	void skosenieSmerY();
	void skos();
	void preklop();
	void kresliOs();
	void otoc();
	void otocenieSmerHodiniek();
	void otocenieProtiSmerHodiniek();

private:
	Ui::MyPainterClass ui;
	PaintWidget paintWidget;
};

#endif // MYPAINTER_H
