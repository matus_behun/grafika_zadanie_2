#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include "hrana.h"
#include "kruh.h"
#include "polygon.h"
#include "grafickyUtvar.h"

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <math.h>
#include <QPoint>
#include <QVector>
#include <QPoint>
#include <QtAlgorithms>
#include <QMessageBox>
#include <QMutableListIterator>
#include <QListIterator>

class PaintWidget : public QWidget
{
	Q_OBJECT

	public:
		PaintWidget(QWidget *parent = 0);
		bool newImage(int x, int y);
		void setColor(QColor);
		void setFillingColor(QColor);
		void kresliCiaru(char algoritmus);
		void vyplnUzavretuOblast(QList<hrana> hrany, QColor c);
		void kresliKruh(int, char, double);
		void kresliMnohouholnik(char);
		void skaluj(double);
		void skos(char, double);
		void otoc(char, double);
		void kresliOs(char algoritmus);
		void preklop(void);

		bool maOs = false;
	protected:
		void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE; 
		void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE; 
		void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
		void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;

	private:
		QImage image;
		QColor color = Qt::black;
		QColor fillingColor = Qt::white;
		QColor farbaOhranicenia = Qt::blue;
		grafickyUtvar *oznaceny = NULL;
		QPoint lastMovedMousePosition;
		hrana os;

		QVector<QPoint> clicks;
		QList<kruh> vykresleneKruhy;
		QList<polygon> vykreslenePolygony;
		QList<grafickyUtvar *> vykresleneUtvary;

		void resizeImage(QImage *image, const QSize &newSize);
		void bresenham(QPoint, QPoint, QColor);
		void dda(QPoint, QPoint, QColor);
		void vykresliVodorovnuCiaru(int, int, int, QColor);
		void vykresliVertikalnuCiaru(int x, int y, int uroven, QColor farba);
		void vykresliOznacenie(grafickyUtvar *, QColor);
		void kresliKruh(kruh, QColor, QColor);
		void kresliMnohouholnik(polygon, QColor, QColor);
		void zmazOs(char algoritmus = 'd');
};

#endif // PAINTWIDGET_H