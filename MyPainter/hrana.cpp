﻿#include "hrana.h"

hrana::hrana()
{
	a = QPoint(0, 0);
	b = QPoint(0, 0);
}

hrana::hrana(QPoint x, QPoint y)
{
		/* dolny y uloz do a*/
	if (x.y() > y.y()) {
		this->a = x;
		this->b = y;
	}
	else {
		this->a = y;
		this->b = x;
	}

	smernica = double(abs(x.x() - y.x())) / double(abs(x.y() - y.y()));
	priesecnik = double(smernica * double(dajDolneY())) - double(smernica * double(dajDolneY() - 1)) + double(dajDolneX());

		/* nasa usecka ide zprava do lava takze hodnoty budeme musiet odpocitavat predpokladom je ze bod 'a' je dolny bod */
	if (a.x() > b.x()) {
		smernica *= -1.0;
	}
}

double hrana::dajPriesecnik(void)
{
	return priesecnik;
}

double hrana::aktualizujPriesecnik(void)
{
	double staryPriesecnik = priesecnik;
	priesecnik += smernica;

	return staryPriesecnik;
}

int hrana::dajHorneX(void) const 
{
	return b.x();
}

int hrana::dajHorneY(void) const
{
	return b.y();
}

int hrana::dajDolneY(void) const
{
	return a.y();
}

int hrana::dajDolneX(void) const
{
	return a.x();
}

void hrana::presunX(int delta)
{
	a.setX(a.x() + delta);
	b.setX(b.x() + delta);
	priesecnik = double(smernica * double(dajDolneY())) - double(smernica * double(dajDolneY() - 1)) + double(dajDolneX());
}

void hrana::presunY(int delta)
{
	a.setY(a.y() + delta);
	b.setY(b.y() + delta);
	priesecnik = double(smernica * double(dajDolneY())) - double(smernica * double(dajDolneY() - 1)) + double(dajDolneX());
}

void hrana::zmenA(QPoint x)
{
	a = x;
}

void hrana::zmenB(QPoint x)
{
	b = x;
}

void hrana::nastavHranu(QPoint x, QPoint y)
{
		/* dolny y uloz do a*/
	if (x.y() > y.y()) {
		this->a = x;
		this->b = y;
	}
	else {
		this->a = y;
		this->b = x;
	}

	smernica = double(abs(x.x() - y.x())) / double(abs(x.y() - y.y()));
	priesecnik = double(smernica * double(dajDolneY())) - double(smernica * double(dajDolneY() - 1)) + double(dajDolneX());

		/* nasa usecka ide zprava do lava takze hodnoty budeme musiet odpocitavat predpokladom je ze bod 'a' je dolny bod */
	if (a.x() > b.x()) {
		smernica *= -1.0;
	}
}

QPoint hrana::dajA(void)
{
	return a;
}

QPoint hrana::dajB(void)
{
	return b;
}

double hrana::dajSmernicu(void) const
{
	return smernica;
}

bool hrana::jeVodorovna(QPoint a, QPoint b)
{
	if (a.y() == b.y()) {
		return true;
	}
	else {
		return false;
	}
}

void hrana::vypis(void)
{
	printf("[%d, %d]->[%d, %d] - %lf\n", dajDolneX(), dajDolneY(), dajHorneX(), dajHorneY(), dajPriesecnik());
}

hrana::~hrana() 
{
	
}

