#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(750, 750);
}

void MyPainter::vyberFarby()
{
	QColorDialog colorDialog;
	paintWidget.setColor(colorDialog.getColor());
}

void MyPainter::vyberFarbyVyplne()
{
	QColorDialog colorDialog;
	paintWidget.setFillingColor(colorDialog.getColor());
}

void MyPainter::vykresliCiaru()
{
	if (ui.radioButton->isChecked()) {
		paintWidget.kresliCiaru('d');
		ui.statusBar->showMessage("DDA ciara");
	} else if (ui.radioButton_2->isChecked()) {
		ui.statusBar->showMessage("Bresenham ciara");
		paintWidget.kresliCiaru('b');
	}
}

void MyPainter::vykresliKruh()
{
	if (ui.radioButton->isChecked()) {
		paintWidget.kresliKruh(ui.spinBox->value(), 'd', ui.doubleSpinBox->value());
		ui.statusBar->showMessage("DDA Kruh");
	} else if (ui.radioButton_2->isChecked()) {
		ui.statusBar->showMessage("Bresenham kruh");
		paintWidget.kresliKruh(ui.spinBox->value(), 'b', ui.doubleSpinBox->value());
	}
}

void MyPainter::vykresliMnohouholnik()
{
	if (ui.radioButton->isChecked()) {
		paintWidget.kresliMnohouholnik('d');
		ui.statusBar->showMessage("DDA mnohouholnik");
	} else if (ui.radioButton_2->isChecked()) {
		ui.statusBar->showMessage("Bresenham mnohouholnik");
		paintWidget.kresliMnohouholnik('b');
	}
}

void MyPainter::skaluj() 
{
	paintWidget.skaluj(ui.doubleSpinBox_2->value());
}

void MyPainter::skosenieSmerX()
{
	ui.radioButton_4->setChecked(false);
}

void MyPainter::skosenieSmerY()
{
	ui.radioButton_3->setChecked(false);
}

void MyPainter::skos()
{
		/* v smere x */
	if (ui.radioButton_3->isChecked()) {
		paintWidget.skos('x', ui.doubleSpinBox_3->value());
		/* v smere y */
	} else if (ui.radioButton_4->isChecked()) {
		paintWidget.skos('y', ui.doubleSpinBox_3->value());
	}
}

void MyPainter::otoc()
{
		/* V smere hodinovych ruciciek */
	if (ui.radioButton_5->isChecked()) {
		paintWidget.otoc('v', ui.doubleSpinBox_4->value());
		/* Proti smeru hodinovych ruciciek */
	} else if (ui.radioButton_6->isChecked()) {
		paintWidget.otoc('p', ui.doubleSpinBox_4->value());
	}
}

void MyPainter::otocenieSmerHodiniek()
{
	ui.radioButton_5->setChecked(false);
}

void MyPainter::otocenieProtiSmerHodiniek()
{
	ui.radioButton_6->setChecked(false);
}

void MyPainter::kresliOs()
{
	if (ui.radioButton->isChecked()) {
		paintWidget.kresliOs('d');
	} else if (ui.radioButton_2->isChecked()) {
		paintWidget.kresliOs('b');
	}
}

void MyPainter::preklop()
{
	if (paintWidget.maOs) {
		paintWidget.preklop();
	}
}

MyPainter::~MyPainter()
{
}

